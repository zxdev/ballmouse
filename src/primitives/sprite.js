import {Vector} from '../types.js'
/**
 * Sprite node
 * @extends Node
 * @param {Object}  options            Options for the node; See below
 * @param {Vector}  options.position   Position vector, defaults to `(0,0)`
 * @param {number}  options.rotation   (Clockwise) Rotation in radians, defaults to `0`
 * @param {Image}   options.image      An image, either a `canvas`, `Image` or `<img>`. Size information will be discarded.
 * @param {Vector}  options.size       Size Vector, defaults to whatever size the image source is
 * @param {Object=} options.region       The section of the image to draw
 * @param {Vector=} options.region.begin Top left corner of the clipping region
 * @param {Vector=} options.region.end   Bottom right corner of the clipping region
 */
class Sprite extends Node {
    render(ctx) {
        if(!options.rect) {
            
        } else {

        }
    }
}

export default Sprite