# entry.js
<a name="render"></a>

## render(time)
The entry render function, used only in this file for requestAnimationFrame

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| time | <code>number</code> | Time in ms since first frame rendered. This is automatically inserted by `requestAnimationFrame` |

