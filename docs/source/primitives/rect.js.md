# rect.js
<a name="Rect"></a>

## Rect ⇐ <code>Node</code>
Rectangle Node

**Kind**: global class  
**Extends**: <code>Node</code>  
<a name="new_Rect_new"></a>

### new Rect(options)

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Options for the node; See below |
| options.position | <code>Vector</code> | Position vector, defaults to `(0,0)` |
| options.rotation | <code>number</code> | (Clockwise) Rotation in radians, defaults to `0` |
| options.size | <code>Vector</code> | How wide the rectangle should be, defaults to `(0,0)` (zero size; invisible)) |
| options.fill | <code>string</code> | What the rectangle should be filled with, defaults to `#000` (solid black) |

