# base.js
<a name="Game"></a>

## Game
The main game class, a lot of logic will happen here

**Kind**: global class  
<a name="new_Game_new"></a>

### new Game(time)
The main tick function, called every frame.


| Param | Type | Description |
| --- | --- | --- |
| time | <code>Number</code> | Time in ms from the first run (not really but close enough) |

